package ru.teterin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.constant.Constant;

import java.util.Comparator;

import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.util.DateUuidParseUtil;

import java.util.Collection;
import java.util.Date;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectService(
        @NotNull final IProjectRepository projectRepository,
        @NotNull final ITaskRepository taskRepository
    ) {
        super(projectRepository);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public Project remove(
        @Nullable final String userId,
        @Nullable final String id
    ) {
        @NotNull final Project project = super.remove(userId, id);
        if (userId != null && id != null) {
            taskRepository.removeAllByProjectId(userId, id);
        }
        return project;
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        super.removeAll(userId);
        if (userId != null) {
            taskRepository.removeAll(userId);
        }
    }

    @NotNull
    @Override
    public Project checkProject(
        @Nullable final String userId,
        @NotNull final Project project
    ) {
        if (userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        @Nullable final String name = project.getName();
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        @Nullable final String description = project.getDescription();
        if (description == null) {
            throw new IllegalArgumentException(Constant.EMPTY_OBJECT);
        }
        project.setUserId(DateUuidParseUtil.isUuid(userId));
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Collection<Task> findAllTaskByProjectId(
        @Nullable final String userId,
        @Nullable final String id
    ) {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        @NotNull final Project project = findOne(DateUuidParseUtil.isUuid(userId), DateUuidParseUtil.isUuid(id));
        return taskRepository.findAllByProjectId(userId, id);
    }

    @NotNull
    @Override
    public Collection<Project> findAndSortAll(
        @Nullable final String userId,
        @Nullable String sortOption
    ) {
        if (userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        DateUuidParseUtil.isUuid(userId);
        if (sortOption == null) {
            sortOption = Constant.CREATE_ASC_SORT;
        }
        @NotNull final Comparator<Project> comparator = createComparator(sortOption);
        @NotNull final Collection<Project> projects = projectRepository.findAndSortAll(userId, comparator);
        return projects;
    }

    @Override
    @NotNull
    public Comparator<Project> createComparator(@NotNull final String sortOption) {
        @NotNull final Comparator<Project> comparator = (Project first, Project second) -> {
            switch (sortOption) {
                default:
                    @NotNull Date firstCreateDate = first.getDateCreate();
                    @NotNull Date secondCreateDate = second.getDateCreate();
                    return firstCreateDate.compareTo(secondCreateDate);
                case Constant.CREATE_DESC_SORT:
                    firstCreateDate = first.getDateCreate();
                    secondCreateDate = second.getDateCreate();
                    return secondCreateDate.compareTo(firstCreateDate);
                case Constant.START_DATE_ASC_SORT:
                    @NotNull Date firstStartDate = first.getDateStart();
                    @NotNull Date secondStartDate = second.getDateStart();
                    return firstStartDate.compareTo(secondStartDate);
                case Constant.START_DATE_DESC_SORT:
                    firstStartDate = first.getDateStart();
                    secondStartDate = second.getDateStart();
                    return secondStartDate.compareTo(firstStartDate);
                case Constant.END_DATE_ASC_SORT:
                    @NotNull Date firstEndDate = first.getDateEnd();
                    @NotNull Date secondEndDate = second.getDateEnd();
                    return firstEndDate.compareTo(secondEndDate);
                case Constant.END_DATE_DESC_SORT:
                    firstEndDate = first.getDateEnd();
                    secondEndDate = second.getDateEnd();
                    return secondEndDate.compareTo(firstEndDate);
                case Constant.STATUS_ASC_SORT:
                    return first.getStatus().compareTo(second.getStatus());
                case Constant.STATUS_DESC_SORT:
                    return second.getStatus().compareTo(first.getStatus());
            }
        };
        return comparator;
    }

    @NotNull
    @Override
    public Collection<Project> searchByString(
        @Nullable final String userId,
        @Nullable String searchString
    ) {
        if (userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        DateUuidParseUtil.isUuid(userId);
        if (searchString == null) {
            searchString = "";
        }
        @NotNull final Collection<Project> projects = projectRepository.searchByString(userId, searchString);
        return projects;
    }

}
