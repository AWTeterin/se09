package ru.teterin.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.enumerated.Role;
import ru.teterin.tm.exception.ObjectNotFoundException;

import java.util.LinkedHashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public final class StateService implements IStateService {

    @Nullable
    private String userId;

    @Nullable
    private String projectId;

    @Nullable
    private String login;

    @Nullable
    private Role role;

    @NotNull
    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    @Override
    public AbstractCommand getCommand(@Nullable final String commandName) {
        if (commandName == null || commandName.isEmpty()) {
            throw new IllegalArgumentException(Constant.NO_COMMAND);
        }
        @Nullable final AbstractCommand command = commands.get(commandName);
        if (command == null) {
            throw new ObjectNotFoundException(Constant.INCORRECT_COMMAND);
        }
        return command;
    }

    @Override
    public void registryCommand(@NotNull final AbstractCommand command) {
        @Nullable final String commandName = command.getName();
        @Nullable final String commandDescription = command.getDescription();
        final boolean noCommandName = commandName.isEmpty();
        final boolean noCommandDescription = commandDescription.isEmpty();
        if (noCommandName || noCommandDescription) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        commands.put(command.getName(), command);
    }

}
