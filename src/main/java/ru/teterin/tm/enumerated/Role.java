package ru.teterin.tm.enumerated;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.Nullable;

@AllArgsConstructor
public enum Role {

    USER("USER"),
    ADMIN("ADMINISTRATOR");

    @Nullable
    private final String name;

    @Nullable
    public String displayName() {
        return name;
    }

}
