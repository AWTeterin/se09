package ru.teterin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all task.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.TASK_CLEAR);
        stateService = serviceLocator.getStateService();
        @Nullable final String userId = stateService.getUserId();
        taskService = serviceLocator.getTaskService();
        taskService.removeAll(userId);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
