package ru.teterin.tm.command;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.*;
import ru.teterin.tm.enumerated.Role;
import ru.teterin.tm.exception.ObjectNotFoundException;
import ru.teterin.tm.exception.ObjectExistException;

public abstract class AbstractCommand {

    @Setter
    @NotNull
    protected ITerminalService terminalService;

    @Setter
    @NotNull
    protected IServiceLocator serviceLocator;

    @Nullable
    protected IProjectService projectService;

    @Nullable
    protected ITaskService taskService;

    @Nullable
    protected IUserService userService;

    @Nullable
    protected IStateService stateService;

    public AbstractCommand() {
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws IllegalArgumentException, ObjectExistException, ObjectNotFoundException;

    public abstract boolean secure();

    public Role[] roles() {
        return null;
    }

}
