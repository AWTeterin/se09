package ru.teterin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.User;
import ru.teterin.tm.enumerated.Role;

public final class UserLogInCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Log in to the task manager.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.USER_LOGIN);
        terminalService.print(Constant.ENTER_LOGIN);
        @Nullable final String login = terminalService.readString();
        terminalService.print(Constant.ENTER_PASSWORD);
        @Nullable final String password = terminalService.readString();
        userService = serviceLocator.getUserService();
        @NotNull final User user = userService.checkUser(login, password);
        @NotNull final String userId = user.getId();
        stateService = serviceLocator.getStateService();
        stateService.setUserId(userId);
        stateService.setLogin(login);
        @Nullable final Role role = user.getRole();
        stateService.setRole(role);
        @NotNull final String helloMessage = Constant.HELLO + login;
        terminalService.print(helloMessage);
    }

    @Override
    public boolean secure() {
        return false;
    }

}
