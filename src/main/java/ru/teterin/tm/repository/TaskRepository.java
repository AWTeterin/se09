package ru.teterin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.entity.Task;

import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void removeAllByProjectId(
        @NotNull final String userId,
        @NotNull final String projectId
    ) {
        @NotNull final Collection<Task> tasks = findAll(userId);
        for (@NotNull final Task task : tasks) {
            @Nullable final String taskProjectId = task.getProjectId();
            if (taskProjectId != null && taskProjectId.equals(projectId)) {
                @NotNull final String id = task.getId();
                entities.remove(id);
            }
        }
    }

    @NotNull
    @Override
    public Collection<Task> findAllByProjectId(
        @NotNull final String userId,
        @NotNull final String projectId
    ) {
        @NotNull final Collection<Task> result = new LinkedHashSet<>();
        for (@NotNull final Task task : findAll(userId)) {
            @Nullable final String taskProjectId = task.getProjectId();
            if (taskProjectId != null && taskProjectId.equals(projectId)) {
                result.add(task);
            }
        }
        return result;
    }

    @Override
    @NotNull
    public Collection<Task> findAndSortAll(
        @NotNull final String userId,
        @NotNull final Comparator<Task> comparator
    ) {
        @NotNull final List<Task> tasks = findAll(userId);
        if (tasks.isEmpty() || tasks.size() == 1) {
            return tasks;
        }
        tasks.sort(comparator);
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> searchByString(
        @NotNull final String userId,
        @NotNull final String searchString
    ) {
        @NotNull final Collection<Task> tasks = findAll(userId);
        @NotNull final Collection<Task> result = new LinkedHashSet<>();
        for (@NotNull final Task task : tasks) {
            @NotNull final String name = task.getName();
            if (name.contains(searchString)) {
                result.add(task);
                continue;
            }
            @NotNull final String description = task.getDescription();
            if (description.contains(searchString)) {
                result.add(task);
            }
        }
        return result;
    }

}
