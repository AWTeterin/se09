package ru.teterin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.IRepository;
import ru.teterin.tm.entity.AbstractEntity;
import ru.teterin.tm.exception.ObjectExistException;

import java.util.*;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @NotNull
    protected final Map<String, T> entities = new LinkedHashMap<>();

    @Nullable
    @Override
    public T findOne(
        @NotNull final String userId,
        @NotNull final String id
    ) {
        @Nullable final T t = entities.get(id);
        if (t == null || t.getUserId() == null || !t.getUserId().equals(userId)) {
            return null;
        }
        return t;
    }

    @NotNull
    @Override
    public List<T> findAll(@NotNull final String userId) {
        @NotNull final List<T> result = new LinkedList<>();
        for (@NotNull final T t : entities.values()) {
            @Nullable final String tUserId = t.getUserId();
            @NotNull final boolean noUserId = tUserId == null || tUserId.isEmpty();
            if (tUserId != null && (!noUserId || tUserId.equals(userId))) {
                result.add(t);
            }
        }
        return result;
    }

    @Nullable
    @Override
    public T persist(
        @NotNull final String userId,
        @NotNull final T t
    ) {
        final boolean hasT = findOne(userId, t.getId()) != null;
        if (hasT) {
            throw new ObjectExistException();
        }
        return entities.put(t.getId(), t);
    }

    @Nullable
    @Override
    public T merge(
        @NotNull final String userId,
        @NotNull final T t
    ) {
        return entities.put(t.getId(), t);
    }

    @Nullable
    @Override
    public T remove(
        @NotNull final String userId,
        @NotNull final String id
    ) {
        @Nullable final T t = findOne(userId, id);
        if (t == null) {
            return null;
        }
        return entities.remove(id);
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final Collection<T> entitiesById = findAll(userId);
        for (@NotNull final T t : entitiesById) {
            entities.remove(t.getId());
        }
    }

}
