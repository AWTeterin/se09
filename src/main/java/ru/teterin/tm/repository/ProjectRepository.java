package ru.teterin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.entity.Project;

import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Collection<Project> findAndSortAll(
        @NotNull final String userId,
        @NotNull final Comparator<Project> comparator
    ) {
        @NotNull final List<Project> projects = findAll(userId);
        if (projects.isEmpty() || projects.size() == 1) {
            return projects;
        }
        projects.sort(comparator);
        return projects;
    }

    @NotNull
    @Override
    public Collection<Project> searchByString(
        @NotNull final String userId,
        @NotNull final String searchString
    ) {
        @NotNull final Collection<Project> projects = findAll(userId);
        @NotNull final Collection<Project> result = new LinkedHashSet<>();
        for (@NotNull final Project project : projects) {
            @NotNull final String name = project.getName();
            if (name.contains(searchString)) {
                result.add(project);
                continue;
            }
            @NotNull final String description = project.getDescription();
            if (description.contains(searchString)) {
                result.add(project);
            }
        }
        return result;
    }

}
