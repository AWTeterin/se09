package ru.teterin.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
public final class Task extends AbstractEntity {

    @NotNull
    private String projectId = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date dateCreate = new Date();

    @NotNull
    private Date dateStart = new Date();

    @NotNull
    private Date dateEnd = new Date();

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    @Override
    public String toString() {
        return "Task{" +
            "id='" + id + '\'' +
            ", projectId='" + projectId + '\'' +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", dateStart=" + Constant.DATE_FORMAT.format(dateStart) +
            ", dateEnd=" + Constant.DATE_FORMAT.format(dateEnd) +
            ", status=" + status +
            '}';
    }

}
