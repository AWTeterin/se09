package ru.teterin.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.enumerated.Role;

@Getter
@Setter
public final class User extends AbstractEntity {

    @Nullable
    private String password;

    @Nullable
    private Role role = Role.USER;

}
