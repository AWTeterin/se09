package ru.teterin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEntity {

    @NotNull
    protected String id = UUID.randomUUID().toString();

    @NotNull
    protected String userId = "";

    @NotNull
    protected String name = "";

}
